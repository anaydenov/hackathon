﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Hackathon.Entities;

namespace Hackathon.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Leaderboard(bool isPrivate = false, bool isTotal = false)
        {
            ViewBag.isPrivate = isPrivate;
            ViewBag.isTotal = isTotal;
            return View();
        }

        /*[Authorize(Roles = "admin")]
        public ActionResult LeaderboardPrivate()
        {
            return View();
        }*/

        public ActionResult Over()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}