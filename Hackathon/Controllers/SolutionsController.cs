﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Threading.Tasks;
using Hackathon.Models;
using Hackathon.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Deedle;

namespace Hackathon.Controllers
{
    public class ScoreResult
    {
        public enum ScoreResultType
        {
            Ok,
            Error
        }

        public ScoreResultType Type;
        public double Score;
        public List<string> Errors;
    }

    public class SolutionsController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Solutions()
        {
            List<Solution> model = new List<Solution>();
            return View(model);
        }

        [Authorize]
        public ActionResult Submit()
        {
            //if (DateTime.UtcNow > new DateTime(2017,6,29,14,45,0,DateTimeKind.Utc))
              //  return RedirectToAction("Over","Home");
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Submit(SubmissionModel model)
        {
            //if (DateTime.UtcNow > new DateTime(2017, 6, 29, 14, 45, 0, DateTimeKind.Utc))
            //    return RedirectToAction("Over", "Home");

            if (!ModelState.IsValid)
            {
                //ScoreResult error = new ScoreResult();
                //error.Errors = new List<string>();

                return View("Submit");
            }

            
            using (var db = new HackathonContext())
            {
                Solution sub = new Solution();
                sub.SubmissionId = Guid.NewGuid();
                sub.Title = model.Title;
                if (Request.IsAuthenticated)
                {
                    sub.UserName = User.Identity.Name;
                }
                else
                    sub.UserName = "TestUser";

                using (var ms = new FileStream($"{SubmissionsPath}{sub.SubmissionId}.csv",FileMode.Create, FileAccess.Write))
                {
                    await model.SolutionFile.InputStream.CopyToAsync(ms);
                }

                //Task.Run(() => model.SolutionFile.InputStream.SaveAs($"{SubmissionsPath}{sub.SubmissionId}.csv"));

                ScoreResult scoreResult;

                scoreResult = Task.Run(() => Score(sub.SubmissionId, true)).Result;
                if (scoreResult.Type != ScoreResult.ScoreResultType.Ok)
                    return View("SubmissionError", scoreResult);
                sub.ScorePublic = scoreResult.Score;

                scoreResult = Task.Run(() => Score(sub.SubmissionId, false)).Result;
                if (scoreResult.Type != ScoreResult.ScoreResultType.Ok)
                    return View("SubmissionError", scoreResult);
                sub.ScorePrivate = scoreResult.Score;

                sub.Submited = DateTime.UtcNow.AddHours(3);

                db.Solutions.Add(sub);
                db.SaveChanges();
            }

            return RedirectToAction("Leaderboard", "Home");
        }

        public ActionResult Leaderboard(bool isPrivate = false, bool isTotal = false)
        {
            using (var db = new HackathonContext())
            {
                LeaderboardModel model = new LeaderboardModel();
                isPrivate = isPrivate && User.IsInRole("admin");
                isTotal = isPrivate && isTotal;
                model.IsPrivate = isPrivate;
                model.IsTotal = isTotal;
                model.Items = db.Solutions.GroupBy(g=>g.UserName)
                                            .Select(g => new {
                                                UserName = g.Key,
                                                Solution = g.OrderByDescending(s => (isPrivate ? (isTotal ? s.ScorePublic*0.2 + s.ScorePrivate*0.8 : s.ScorePrivate) : s.ScorePublic)).FirstOrDefault(),
                                                SubmissionsCount = g.Count()
                                            })
                                            .OrderByDescending(s => (isPrivate ? 
                                                        (isTotal ? 
                                                            s.Solution.ScorePrivate*0.8 + s.Solution.ScorePublic*0.2 : 
                                                            s.Solution.ScorePrivate) : 
                                                        s.Solution.ScorePublic))
                                            .ToList()
                                            .Select(s => {
                                                var user = UserManager.FindByNameAsync(s.UserName).Result;
                                                LeaderboardItemModel li = new LeaderboardItemModel()
                                                {
                                                    UserName = s.UserName,
                                                    Surname = user?.Surname,
                                                    Name = user?.Name,
                                                    TerBank = user?.TerBank.ToString(),
                                                    ScorePublic = s.Solution.ScorePublic,
                                                    ScorePrivate = s.Solution.ScorePrivate,
                                                    ScoreTotal = s.Solution.ScorePublic*0.2 + s.Solution.ScorePrivate*0.8,
                                                    Submited = s.Solution.Submited,
                                                    Title = s.Solution.Title,
                                                    SubmissionsCount = s.SubmissionsCount
                                                };
                                                return li;
                                            })
                                            .ToList();
                //if (isPrivate)
                //    return View("LeaderboardPrivate", model);
                //else
                    return View(model);
            }
        }

        [Authorize(Roles = "admin")]
        public ActionResult LeaderboardTB(bool isTotal = false)
        {
            using (var db = new HackathonContext())
            {
                LeaderboardTBModel model = new LeaderboardTBModel();
                bool isPrivate = true;
                //bool isTotal = true;
                /*model.IsPrivate = isPrivate;
                model.IsTotal = isTotal;*/
                var lbUsers = db.Solutions.GroupBy(g => g.UserName)
                                            .Select(g => new {
                                                UserName = g.Key,
                                                Solution = g.OrderByDescending(s => (isPrivate ? (isTotal ? s.ScorePublic * 0.2 + s.ScorePrivate * 0.8 : s.ScorePrivate) : s.ScorePublic)).FirstOrDefault(),
                                                SubmissionsCount = g.Count()
                                            })
                                            .OrderByDescending(s => (isPrivate ?
                                                        (isTotal ?
                                                            s.Solution.ScorePrivate * 0.8 + s.Solution.ScorePublic * 0.2 :
                                                            s.Solution.ScorePrivate) :
                                                        s.Solution.ScorePublic))
                                            .ToList()
                                            .Select(s => {
                                                var user = UserManager.FindByNameAsync(s.UserName).Result;
                                                LeaderboardItemModel li = new LeaderboardItemModel()
                                                {
                                                    UserName = s.UserName,
                                                    Surname = user?.Surname,
                                                    Name = user?.Name,
                                                    TerBank = user?.TerBank.ToString(),
                                                    ScorePublic = s.Solution.ScorePublic,
                                                    ScorePrivate = s.Solution.ScorePrivate,
                                                    ScoreTotal = s.Solution.ScorePublic * 0.2 + s.Solution.ScorePrivate * 0.8,
                                                    Submited = s.Solution.Submited,
                                                    Title = s.Solution.Title,
                                                    SubmissionsCount = s.SubmissionsCount
                                                };
                                                return li;
                                            })
                                            .ToList();

                model.Items = lbUsers.GroupBy(g => g.TerBank)
                                    .Select(g => new LeaderboardBaseItemModel()
                                    {
                                        TerBank = g.Key,
                                        ScorePublic = g.Average(u => u.ScorePublic),
                                        ScorePrivate = g.Average(u => u.ScorePrivate),
                                        ScoreTotal = g.Average(u => u.ScorePublic*0.2) + g.Average(u => u.ScorePrivate*0.8),
                                        SubmissionsCount = g.Sum(u => u.SubmissionsCount)
                                    })
                                    .OrderByDescending(g => isTotal ? g.ScoreTotal : g.ScorePrivate)
                                    .ToList();
                //if (isPrivate)
                //    return View("LeaderboardPrivate", model);
                //else
                return View(model);
            }
        }


        /*[Authorize(Roles = "admin")]
        public ActionResult LeaderboardPrivate()
        {
            using (var db = new HackathonContext())
            {
                List<Solution> model = db.Solutions.OrderByDescending(s => s.ScorePrivate).ToList();

                return View(model);
            }
        }*/

        public ScoreResult Score(Guid submissionFileId, bool isPublic)
        {
            string valtestname = isPublic ? "test" : "val";
            var valtest = Frame.ReadCsv($"{DataPath}short_{valtestname}.csv")
                    .IndexRows<int>("Id");
            valtest.RenameColumns(s => "vt" + s);


            var submission = Frame.ReadCsv($"{SubmissionsPath}{submissionFileId}.csv").IndexRows<int>("Id");
            submission.RenameColumns(s => "s" + s);

            var all = valtest.Join(submission, JoinKind.Left);
            //all.SaveCsv($"{DataPath}all.csv");

            Metrics.MetricResult metricResult = (new Metrics()).Score(all, "vtClass", "sClass", Metrics.MetricType.F1);

            return new ScoreResult()
            {
                Type = metricResult.IsOk ? ScoreResult.ScoreResultType.Ok : ScoreResult.ScoreResultType.Error,
                Score = metricResult.Score,
                Errors = metricResult.Errors
            };
        }

        private string DataPath
        {
            get
            {
                return this.Server.MapPath("~/App_Data/");
            }
        }

        private string SubmissionsPath
        {
            get
            {
                return DataPath + "Submissions/";
            }
        }

        public ActionResult DeleteAllSubmissions()
        {
            using (var db = new HackathonContext())
            {
                foreach (var sub in db.Solutions)
                    db.Solutions.Remove(sub);
                db.SaveChanges();
            }
            return RedirectToAction("Index", "Home");
        }
    }
}