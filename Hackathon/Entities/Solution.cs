﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Hackathon.Entities
{
    public class Solution
    {
        [Key]
        public Guid SubmissionId { get; set; }
        [Required]
        public string Title { get; set; }
        public double ScorePublic { get; set; }
        public double ScorePrivate { get; set; }
        public string UserName { get; set; }

        public DateTime Submited { get; set; }
    }

    public class HackathonContext : DbContext
    {
        public HackathonContext()
            : base("DefaultConnection")//, throwIfV1Schema: false)
        {
        }

        public DbSet<Solution> Solutions { get; set; }
    }
}