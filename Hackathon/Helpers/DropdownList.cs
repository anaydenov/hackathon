﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using Hackathon.Models;
using EnumAnnotations;

namespace System.Web.Mvc.Html
{
    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString EnumDropDownListFor<TModel, TProperty, TEnum>(
                    this HtmlHelper<TModel> htmlHelper,
                    Expression<Func<TModel, TProperty>> expression,
                    TEnum selectedValue)
        {
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum))
                                        .Cast<TEnum>();
            IEnumerable<SelectListItem> items = from value in values
                                                select new SelectListItem()
                                                {
                                                    Text = Enum.GetName(typeof(TEnum), value),
                                                    Value = value.ToString(),
                                                    Selected = (value.Equals(selectedValue))
                                                };
            return SelectExtensions.DropDownListFor(htmlHelper, expression, items);
        }

        /*public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<TModel> T)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var tb in terBanks)
            {
                SelectListItem item = new SelectListItem() { Value = tb.ToString(), Text = EnumAnnotation.GetNames< };
                selectList.Add(item);
            }
            return SelectExtensions.DropDownListFor(htmlHelper, expression, selectList);
        }
        
        /*public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<TerBank> terBanks, object htmlAttributes)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var tb in terBanks)
            {
                SelectListItem item = new SelectListItem() { Value = theme.ThemeSectionId.ToString(), Text = theme.Title };
                selectList.Add(item);
            }
            return SelectExtensions.DropDownListFor(htmlHelper, expression, selectList, htmlAttributes);
        }*/
    }
}
