﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace Hackathon.Models
{
    public enum TerBank
    {
        [Display(Name = "ЦА")]
        CA = 0,
        [Display(Name = "ББ")]
        BB = 1,
        [Display(Name = "ВВБ")]
        VVB = 2,
        [Display(Name = "ДВБ")]
        DVB = 3,
        [Display(Name = "ЗСБ")]
        ZSB = 4,
        [Display(Name = "ЗУБ")]
        ZUB = 5,
        [Display(Name = "МБ")]
        MB = 6,
        [Display(Name = "ПБ")]
        PB = 7,
        [Display(Name = "СевБ")]
        SevB = 8,
        [Display(Name = "СЗБ")]
        SZB = 9,
        [Display(Name = "СибБ")]
        SibB = 10,
        [Display(Name = "УБ")]
        UB = 11,
        [Display(Name = "ЦЧБ")]
        CChB = 12,
        [Display(Name = "ЮЗБ")]
        UZB = 13,
        [Display(Name = "СРБ")]
        SRB = 14
    }
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public TerBank TerBank { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


    }
}