﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hackathon.Models
{
    public class LeaderboardItemModel : LeaderboardBaseItemModel
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        /*public string TerBank { get; set; }
        public int SubmissionsCount { get; set; }*/
        public string Title { get; set; }
        /*8public double ScorePublic { get; set; }
        public double ScorePrivate { get; set; }
        public double ScoreTotal { get; set; }*/
        public string UserName { get; set; }
        public DateTime Submited { get; set; }
    }

    public class LeaderboardBaseItemModel
    {
        public string TerBank { get; set; }
        public int SubmissionsCount { get; set; }
        public double ScorePublic { get; set; }
        public double ScorePrivate { get; set; }
        public double ScoreTotal { get; set; }
    }

    public class LeaderboardModel
    {
        public bool IsPrivate { get; set; }
        public bool IsTotal { get; set; }
        public List<LeaderboardItemModel> Items { get; set; }

        public LeaderboardModel()
        {
            Items = new List<LeaderboardItemModel>();
            IsPrivate = false;
        }
    }

    public class LeaderboardTBModel
    {
        public List<LeaderboardBaseItemModel> Items { get; set; }
        public LeaderboardTBModel()
        {
            Items = new List<LeaderboardBaseItemModel>();
        }
    }
}