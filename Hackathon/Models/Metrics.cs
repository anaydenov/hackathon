﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Deedle;

namespace Hackathon.Models
{
    public class Metrics
    {
        public enum MetricType
        {
            LogLos,
            Precision,
            Recall,
            F1
        }

        public class MetricResult
        {
            public bool IsOk { get; set; }
            public double Score { get; set; }
            public List<string> Errors { get; set; }

            public MetricResult()
            {
                Errors = new List<string>();
                Score = 0;
                IsOk = false;
            }
        }

        private class TFPN
        {
            public int TruePositive;
            public int TrueNegative;
            public int FalsePositive;
            public int FalseNegative;
        }

        public MetricResult Score(Deedle.Frame<int, string> data, string testCol, string predictCol, MetricType metric)
        {
            MetricResult res = Validate(data, testCol, predictCol);
            if (!res.IsOk)
                return res;

            switch (metric)
            {
                case MetricType.F1:
                    return ScoreF1(data, testCol, predictCol);
                case MetricType.Precision:
                    return ScorePrecision(data, testCol, predictCol);
                case MetricType.Recall:
                    return ScoreRecall(data, testCol, predictCol);
                case MetricType.LogLos:
                    return ScoreLogLos(data, testCol, predictCol);
                default:
                    return new MetricResult()
                    {
                        IsOk = false
                    };
            }
        }

        public MetricResult ScoreF1(Deedle.Frame<int, string> data, string testCol, string predictCol)
        {
            var precision = ScorePrecision(data, testCol, predictCol);
            if (!precision.IsOk)
                return precision;

            var recall = ScoreRecall(data, testCol, predictCol);
            if (!recall.IsOk)
                return recall;

            MetricResult res = new MetricResult();
            res.IsOk = true;
            if ((precision.Score + recall.Score) == 0)
                res.Score = 0;
            else
                res.Score = 2 * precision.Score * recall.Score / (precision.Score + recall.Score);
            return res;
        }

        public MetricResult ScoreRecall(Deedle.Frame<int, string> data, string testCol, string predictCol)
        {
            MetricResult res = new MetricResult();

            var tfpn = GetTFPN(data, testCol, predictCol);

            res.IsOk = true;
            if ((double)tfpn.TruePositive + (double)tfpn.FalsePositive == 0)
                res.Score = 0;
            else
                res.Score = ((double)tfpn.TruePositive) / ((double)tfpn.TruePositive + (double)tfpn.FalsePositive);
            return res;
        }

        public MetricResult ScorePrecision(Deedle.Frame<int, string> data, string testCol, string predictCol)
        {
            MetricResult res = new MetricResult();

            var tfpn = GetTFPN(data, testCol, predictCol);

            res.IsOk = true;
            if ((double)tfpn.TruePositive + (double)tfpn.FalseNegative == 0)
                res.Score = 0;
            else
                res.Score = ((double)tfpn.TruePositive) / ((double)tfpn.TruePositive + (double)tfpn.FalseNegative);
            return res;
        }

        private TFPN GetTFPN(Deedle.Frame<int, string> data, string testCol, string predictCol)
        {
            var tfAll = data.Rows.Select(r =>
            {
                var t = (int)Math.Round(r.Value.GetAs<double>(testCol));
                var p = (int)Math.Round(r.Value.GetAs<double>(predictCol));

                return new
                {
                    tpn = ((t == 1) && (p == 1)) ? 1 : 0,
                    tnn = ((t == 0) && (p == 0)) ? 1 : 0,
                    fnn = ((t == 1) && (p == 0)) ? 1 : 0,
                    fpn = ((t == 0) && (p == 1)) ? 1 : 0
                };
            });

            var h = tfAll.Where(t => t.Value.tpn == 1).ValueCount;
            
            return new TFPN()
            {
                TruePositive = tfAll.SelectValues(a => a.tpn).Values.Sum(),
                TrueNegative = tfAll.SelectValues(a => a.tnn).Values.Sum(),
                FalseNegative = tfAll.SelectValues(a => a.fnn).Values.Sum(),
                FalsePositive = tfAll.SelectValues(a => a.fpn).Values.Sum()
            };
        }

        public MetricResult ScoreLogLos(Deedle.Frame<int, string> data, string testCol, string predictCol)
        {
            MetricResult res = new MetricResult();

            var vtclass = data.GetColumn<double>(testCol);
            var sclass = data.GetColumn<double>(predictCol)
                            .Select(v => Math.Max(Math.Min(v.Value, 1 - Math.Pow(10, -15)), Math.Pow(10, -15)));

            int numOfClasses = vtclass.Values.Distinct().Count();
            if (numOfClasses > 2) //multiclass classification
                res.Score = -(vtclass * sclass.Log()).Sum() / sclass.ValueCount;
            else if (numOfClasses == 2) //binary classification
                res.Score = -(vtclass * sclass.Log() + vtclass.Select(v => 1 - v.Value) * sclass.Select(v => 1 - v.Value).Log()).Sum() / vtclass.ValueCount;
            else //unknown classification
            {
                res.IsOk = false;
                return res;
            }
            
            res.IsOk = true;
            return res;
        }

        private MetricResult Validate(Deedle.Frame<int, string> data, string testCol, string predictCol)
        {
            MetricResult res = new MetricResult();
            res.IsOk = false;

            if (data.RowCount==0)
            {
                res.Errors.Add("Направлено пустое множество!");
                return res;
            }

            if (data.GetColumn<double>(testCol).ValueCount != data.GetColumn<double>(predictCol).ValueCount)
            {
                res.Errors.Add("Размер решения не соответствует корректному!");
                return res;
            }

            res.IsOk = true;
            return res;
        }

    }
}