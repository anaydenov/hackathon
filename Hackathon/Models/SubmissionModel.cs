﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Hackathon.Models
{
    public class SubmissionModel
    {
        [Display(Name = "Метка")]
        [Required(ErrorMessage = "Необходимо указать метку решения!")]
        public string Title { get; set; }

        [Display(Name = "Файл решения")]
        [Required(ErrorMessage = "Необходимо добавить файл решения!")]
        public HttpPostedFileBase SolutionFile { get; set; }

        [FileExtensions(ErrorMessage = "Необходимо загрузить csv-файл!", Extensions = "csv")]
        public string FileName
        {
            get
            {
                if (SolutionFile != null)
                    return SolutionFile.FileName;
                else
                    return String.Empty;
            }
        }
    }
}